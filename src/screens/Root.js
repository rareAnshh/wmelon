import React, {Component, useState} from 'react';
import {generateRecords} from '../model/generate';
import {Alert, Button, View} from 'react-native';

import MovieList from '../components/ItemPost';

const Root = ({database, navigation}) => {
  const [isGenerating, setIsGenerating] = useState < Boolean > false;

  const generate = async () => {
    setIsGenerating(true);
    const count = await generateRecords(database);
    Alert.alert(`Generated ${count} records!`);
    setIsGenerating(false);
  };

  return (
    <>
      <View>
        <Button
          title="Generate Records"
          onPress={generate}
          disabled={isGenerating}
        />
      </View>
      {!isGenerating && (
        <MovieList database={database} search="" navigation={navigation} />
      )}
    </>
  );
};

export default Root;
