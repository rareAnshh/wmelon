import {Model} from '@nozbe/watermelondb';
import {field, text} from '@nozbe/watermelondb/decorators';

export default class User extends Model {
  static table = 'users';
  @text('first_name') firstName;
  @text('last_name') lastName;
  @text('email') email;
  @text('password') password;
  @field('is_active') isActive;
}
