// models/Movie.js
import {Model} from '@nozbe/watermelondb';
import * as decorators from '@nozbe/watermelondb/decorators';

export default class Post extends Model {
  static table = 'posts';

  static associations = {
    reviews: {type: 'has_many', foreignKey: 'post_id'},
  };

  @decorators.field('title') title;
  @decorators.field('body') body;
  @decorators.children('comments') comments;

  @decorators.readonly @decorators.date('created_at') createdAt;

  @decorators.readonly @decorators.date('updated_at') updatedAt;

  @decorators.action async getPost() {
    return {
      title: this.title,
      body: this.posterImage,
      comments: this.comments,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
    };
  }

  @decorators.action async updatePost({title, body}) {
    return await this.update(post => {
      post.title = title;
      post.body = body;
    });
  }

  deleteAllComments() {
    return this.comments.destroyAllPermanently();
  }

  @decorators.action async deletePost() {
    await this.deleteAllComments();
    await this.markAsDeleted(); // syncable
    await this.destroyPermanently(); // permanent
  }

  @decorators.action async addComment({body}) {
    return await this.collections.get('comments').create(comment => {
      comment.body = body;
      comment.post.set(this);
      comment.isNasty = false;
    });
  }
}
