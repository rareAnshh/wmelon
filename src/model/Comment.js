// models/Comment.js
import {Model} from '@nozbe/watermelondb';
import * as decorators from '@nozbe/watermelondb/decorators';

export default class Comment extends Model {
  static table = 'comments';

  static associations = {
    movie: {type: 'belongs_to', key: 'post_id'},
  };

  @decorators.field('body') body;
  @decorators.field('is_nasty') isNasty;
  @decorators.field('post_id') postId;
  @decorators.relation('posts', 'post_id') post;
  @decorators.readonly @decorators.date('created_at') createdAt;
  @decorators.readonly @decorators.date('updated_at') updatedAt;

  @decorators.action async deleteComment() {
    return await Promise.all([
      this.markAsDeleted(), // syncable
      this.destroyPermanently(), // permanent
    ]);
  }
}
