import {Database} from '@nozbe/watermelondb';
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite';

import schema from '../model/schema';
// import User from '../model/User';
import Post from '../model/Post';
import Comment from '../model/Comment';
const adapter = new SQLiteAdapter({
  jsi: true,
  dbName: 'WatermelonDemo',
  schema,
});

const database = new Database({
  adapter,
  modelClasses: [Post, Comment],
  actionsEnabled: true,
});

export default {database};
