// models/index.js
import Movie from './Movie';
import Review from './Reviews';

export const dbModels = [Movie, Review];
