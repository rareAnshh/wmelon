import {appSchema, tableSchema} from '@nozbe/watermelondb';

export default appSchema({
  version: 1,
  tables: [
    tableSchema({
      name: 'users',
      columns: [
        {name: 'first_name', type: 'string'},
        {name: 'last_name', type: 'string', isOptional: true},
        {name: 'email', type: 'string'},
        {name: 'password', type: 'string'},
        {name: 'is_active', type: 'boolean'},
      ],
    }),
    tableSchema({
      name: 'posts',
      columns: [
        {name: 'title', type: 'string'},
        {name: 'body', type: 'string'},
        {name: 'created_at', type: 'number'},
        {name: 'updated_at', type: 'number'},
      ],
    }),
    tableSchema({
      name: 'comments',
      columns: [
        {name: 'body', type: 'string'},
        {name: 'author_name', type: 'string'},
        {name: 'post_id', type: 'string', isIndexed: true},
        {name: 'is_nasty', type: 'boolean'},
        {name: 'created_at', type: 'number'},
        {name: 'updated_at', type: 'number'},
      ],
    }),
  ],
});
