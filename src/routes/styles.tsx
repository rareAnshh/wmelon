import {StyleSheet} from 'react-native';

export const style = () => {
  return StyleSheet.create({
    icon: {
      height: 24,
      width: 24,
      marginLeft: 20,
    },
    cross: {
      height: 15,
      width: 15,
      marginLeft: 15,
    },
    selectedItem: {width: '98%', borderRadius: 0, marginLeft: 2},
    row: {flexDirection: 'row'},
    menuText: {alignSelf: 'center', marginHorizontal: 20},
    justifyCenter: {justifyContent: 'center'},
  });
};
